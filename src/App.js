import React from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import {Login} from "./Login";
import {Character} from "./Character";
import {Battle} from "./Battle";

function App() {
  return (
    <BrowserRouter>
      <Route path="/" exact component={Login} />
      <Route path="/character" component={Character} />
      <Route path="/battle" component={Battle} />
    </BrowserRouter>
  );
}

export default App;
