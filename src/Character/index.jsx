import React, {Fragment} from 'react';
import { withRouter } from 'react-router-dom';
import cookies from 'js-cookie';
import axios from 'axios';

import {Container, Row, Col, Accordion, InputGroup, FormControl, Button} from "react-bootstrap";

import classes from './Character.module.scss';
import {SkillProgressBar} from "./SkillProgressBar";

class CharacterPage extends React.Component {
  state = {
    username: '',
    fighter_level: 1,
    current_exp: 0,
    subjects: [],
    battleMode: undefined,
    modalIsOpen: false
  };

  componentDidMount() {
    const token = cookies.get('token');
    if (!token) {
      this.props.history.push('/');
    } else {
      this.updateInfo(token);
    }
  }

  updateInfo = async (token) => {
    try {
      const response = await axios.get('https://digital-vanguard.herokuapp.com/account', {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });

      this.setState(response.data);
    } catch (e) {
      this.props.history.push('/');
      cookies.remove('token');
      console.log(e);
    }
  };

  chooseMode = mode => {
    this.setState({
      battleMode: mode,
      modalIsOpen: true
    });
  };

  enterBattle = subject => {
    let params = new URLSearchParams();
    params.append("subject", subject);
    params.append("mode", this.state.battleMode);
    console.log(params.toString());
    this.props.history.push(`/battle?${params.toString()}`);
  };

  render() {
    return (
      <div className={classes.bodyContainer}>
        <Container>
          <Row>
            <Col className={classes.topMenu}>
              <img
                className={classes.arrow}
                onClick={() => this.chooseMode("training")}
                src={"/images/topMenuLeft.png"}
              />
              <img
                className={classes.gates}
                onClick={() => this.chooseMode("battle")}
                src={"/images/topMenuGates.png"}
              />
              <img className={classes.arrow} src={"/images/topMenuRight.png"}/>
            </Col>
          </Row>
          <Row>
            <Col md={3}>
              <Row>
                <Col className={"mb-3"}>
                  <div className={classes.scrollTop}>
                    <img
                      src={"/images/scrollTop.png"}
                      className={classes.character}
                    />
                  </div>
                  <div className={[classes.scrollMiddle, classes.character].join(" ")}>
                    <p>{this.state.username }</p>
                    <p>{this.state.fighter_level} уровень</p>
                    <img className={classes.characterContainer}
                         src={process.env.PUBLIC_URL + "/images/character.png"}
                         alt="your character"
                    />
                    <div className={classes.hitPointBar}>
                      <img src={"/images/bar-segment-yellow.png"} />
                      <img src={"/images/bar-segment-yellow.png"} />
                      <img src={"/images/bar-segment-yellow.png"} />
                      <img src={"/images/bar-segment-yellow.png"} />
                      <img src={"/images/bar-segment-yellow.png"} />
                      <img src={"/images/bar-segment-yellow.png"} />
                      <img src={"/images/bar-segment-yellow.png"} />
                      <img src={"/images/bar-segment-yellow.png"} />
                      <img src={"/images/bar-segment-yellow.png"} />
                      <img src={"/images/bar-segment-yellow.png"} />
                    </div>
                    <div className={classes.expPointBar}>
                      {Array.apply(0, Array(this.state.current_exp)).map(_ => (
                        <img src={"/images/bar-segment-blue.png"} />
                      ))}
                    </div>
                  </div>
                  <div className={classes.scrollBottom}>
                    <img
                      src={"/images/scrollBottom.png"}
                      className={classes.character}
                    />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col md={8} className={classes.board}>
              <Accordion>
                {this.state.subjects.map((subject, index) => (
                  <Fragment>
                    <Accordion.Toggle
                      as={SkillProgressBar}
                      text={subject.name}
                      amount={subject.overall}
                      animate
                      variant="link"
                      eventKey={index}
                    />
                    <Accordion.Collapse eventKey={index}>
                      <div className={classes.topicProgressBars}>
                        {subject.categories.map(category => (
                          <SkillProgressBar
                            text={category.name}
                            amount={category.score}
                          />
                        ))}
                      </div>
                    </Accordion.Collapse>
                  </Fragment>
                ))}
              </Accordion>
            </Col>
          </Row>
        </Container>
        {this.state.modalIsOpen ? (
          <div className={classes.modalContainer}>

            <div className={classes.modalGroup}>
              <div className={classes.hat}>
                <img
                  src={process.env.PUBLIC_URL + "/images/hat.png"}
                  alt={"hat"}
                  className={classes.modal}
                />
              </div>
              <div className={classes.scrollTop}>
                <img src={"/images/scrollTop.png"} className={classes.modal}/>
              </div>
              <div className={[classes.scrollMiddle, classes.modal].join(" ")}>
                <div className={classes.modalBox}>
                  <div
                    className={[classes.modalButton, classes.mathButton].join(" ")}
                    onClick={() => this.enterBattle("Математика")}
                  />
                  <div
                    className={[classes.modalButton, classes.russianButton].join(" ")}
                    onClick={() => this.enterBattle("Русский язык")}
                  />
                  <div
                    className={[classes.modalButton, classes.informaticsButton].join(" ")}
                    onClick={() => this.enterBattle("Информатика")}
                  />
                  <div
                    className={[classes.modalButton, classes.chemistryButton].join(" ")}
                    onClick={() => this.enterBattle("Химия")}
                  />
                  <div
                    className={[classes.modalButton, classes.biologyButton].join(" ")}
                    onClick={() => this.enterBattle("Биология")}
                  />
                </div>
              </div>
              <div className={classes.scrollBottom}>
                <img src={"/images/scrollBottom.png"} className={classes.modal}/>
              </div>
            </div>
          </div>
        ) : ""}
      </div>
    );
  }
}

export const Character = withRouter(CharacterPage);
