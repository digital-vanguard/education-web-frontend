import React, {useState, useEffect} from 'react';

import classes from './SkillProgressBar.module.scss';

export const SkillProgressBar = props => {
  const [percentage, setPercentage] = useState(0);
  const [barBackgroundImage, setBarBackgroundImage] = useState('');
  const [textBackgroundImage,setTextBackgroundImage] = useState('');

  const updatePercentage = (percentage) => {
    console.log(percentage);
  };

  useEffect(() => {
    if (props.animate) {
      const interval = setInterval(() => {
        setPercentage(prev => {
          if (prev < props.amount) {
            setBarBackgroundImage('linear-gradient(110deg, #00838F ' +
              prev + '%, rgba( 255, 255, 255, 0.9 ) ' + prev + '%)');
            setTextBackgroundImage('linear-gradient(110deg, #FFFFFF ' +
              prev + '%, rgba( 0, 0, 0, 0.7 ) ' + prev + '%)');

            return prev + 1
          } else {
            clearInterval(interval);
            return prev;
          }
        });
      }, 25);
    } else {
      setBarBackgroundImage('linear-gradient(110deg, #00838F ' +
        props.amount + '%, rgba( 255, 255, 255, 0.9 ) ' + props.amount + '%)');
      setTextBackgroundImage('linear-gradient(110deg, #FFFFFF ' +
        props.amount + '%, rgba( 0, 0, 0, 0.7 ) ' + props.amount + '%)');
      setPercentage(props.amount);
    }
  }, {});


  return (
      <div
        className={classes.bar}
        style={{backgroundImage: barBackgroundImage}}
        onClick={props.onClick}
        click={props.click}
      >
        <div className={classes.text} style={{backgroundImage: textBackgroundImage}}>
          {props.text} - {percentage}
        </div>
      </div>
  );
};
