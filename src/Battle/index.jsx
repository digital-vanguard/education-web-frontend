import React, {Fragment} from 'react';
import io from 'socket.io-client';
import {withRouter} from 'react-router-dom';

import {Button, Col, Container, FormControl, Row} from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap'

import classes from './Battle.module.scss';
import cookies from "js-cookie";

const socket = io('https://digital-vanguard-battle.herokuapp.com/');

class BattlePage extends React.Component {
  state = {
    userName: 'Андрей',
    userLevel: '1',
    opponentName: 'Кот',
    opponentLevel: '1',
    tasks: [],
    userTasksStatuses: [],
    opponentTasksStatuses: [],
    currentTask: 0,
    battleType: 'training',
    subject: 'Русский язык',
    ended: false,
    winner: false,
    currentAnswer: '',
    modalIsOpen: false
  };

  componentDidMount() {
    const token = cookies.get('token');
    if (!token) {
      this.props.history.push('/');
    } else {
      const params = new URLSearchParams(this.props.location.search);
      this.setState({
        battleType: params.get("mode"),
        subject: params.get("subject")
      });

      socket.on("auth_result", this.processAuthResult);
      socket.on("fight", this.processFight);
      socket.on("submitted", this.processSubmitted);
      socket.on("stop_battle", this.processStopBattle);
      socket.emit("authenticate", {
        access_token: token
      });
    }
  }

  processAuthResult = (data) => {
    console.log('processAuthResult', data);
    socket.emit('start_battle', {
      battle_type: this.state.battleType,
      subject: this.state.subject
    });
  };

  processFight = (data) => {
    console.log('processFight', data);
    this.setState({
      tasks: data.tasks,
      userTasksStatuses: data.tasks.map(task => "inactive"),
      opponentTasksStatuses: data.tasks.map(task => "inactive"),
      userName: data.self.username,
      userLevel: data.self.fighter_level,
      opponentName: data.opponent.username,
      opponentLevel: data.opponent.fighter_level
    })
  };

  processSubmitted = (data) => {
    console.log('processSubmitted', data);
    if (data.source === "opponent") {
      const opponentTasksStatuses = this.state.opponentTasksStatuses.map((val, ind) => {
        if (ind !== data.index) return val;
        return data.correct ? "success" : "fail";
      });

      this.setState({
        opponentTasksStatuses
      });
    } else {
      const userTasksStatuses = this.state.userTasksStatuses.map((val, ind) => {
        if (ind !== data.index) return val;
        return data.correct ? "success" : "fail";
      });

      let unsolvedInd = 0;
      for (let i = 0; i < userTasksStatuses.length; i++) {
        if (userTasksStatuses[i] === 'inactive') {
          unsolvedInd = i;
          break;
        }
      }

      this.setState({
        currentTask: unsolvedInd,
        currentAnswer: '',
        userTasksStatuses
      });
    }
  };

  processStopBattle = (data) => {
    console.log('processStopBattle', data);
    this.setState({
      ended: true,
      winner: data.winner === "you",
      modalIsOpen: true
    });
  };

  processSubmitClick = () => {
    socket.emit("submit_task", {
      task_index: this.state.currentTask,
      answer: this.state.currentAnswer
    });
  };

  render() {
    return (
      <>
        <div className={classes.topMainContainer}>
          <Container>
            <Row>
              <Col md={2} className={classes.characterContainer}>
                <img
                  src={process.env.PUBLIC_URL + "/images/character.png"}
                  alt="your character"
                />
              </Col>
              <Col md={2} className={classes.characterContainer}>
                <div className={classes.nameBlock}>
                  <p>{this.state.userName}</p>
                  <p>{this.state.userLevel} уровень</p>
                </div>
              </Col>
              <Col md={4} className={classes.titleContainer}>
                {this.state.subject}
              </Col>
              <Col md={2} className={classes.characterContainer}>
                <div className={classes.nameBlockRight}>
                  <p>{this.state.opponentName}</p>
                  <p>{this.state.opponentLevel} уровень</p>
                </div>
              </Col>
              <Col md={2} className={classes.characterContainer}>
                <img
                  src={process.env.PUBLIC_URL + (this.state.battleType === "training" ?
                    "/images/character-scarecrow.png" : "/images/character.png")}
                  alt="your character"
                  className={this.state.battleType === "training" ? classes.scarecrow : ""}
                />
              </Col>
            </Row>
          </Container>
        </div>
        <div className={classes.bottomMainContainer}>
          <Container>
            <Row>
              <Col md={2}>
                <div className={classes.battleProgress}>
                  {this.state.userTasksStatuses.map((status, ind) => {
                    if (this.state.currentTask === ind) {
                      return (
                        <Fragment>
                          <img
                            src={"/images/battleProgress/" + (ind === 0 ? "chain-start.png" : "chain-middle.png")}
                            className={classes.chain}
                          />
                          <div className={classes.ballActive}>{ind + 1}</div>
                        </Fragment>
                      )
                    }
                    switch (status) {
                      case "inactive":
                        return (
                          <Fragment>
                            <img
                              src={"/images/battleProgress/" + (ind === 0 ? "chain-start.png" : "chain-middle.png")}
                              className={classes.chain}
                            />
                            <div className={classes.ballInactive}>{ind + 1}</div>
                          </Fragment>
                        );
                      case "success":
                        return (
                          <Fragment>
                            <img
                              src={"/images/battleProgress/" + (ind === 0 ? "chain-start.png" : "chain-middle.png")}
                              className={classes.chain}
                            />
                            <div className={classes.ballSuccess}>{ind + 1}</div>
                          </Fragment>
                        );
                      case "fail":
                        return (
                          <Fragment>
                            <img
                              src={"/images/battleProgress/" + (ind === 0 ? "chain-start.png" : "chain-middle.png")}
                              className={classes.chain}
                            />
                            <div className={classes.ballFail}>{ind + 1}</div>
                          </Fragment>
                        );
                    }
                  })}
                </div>
              </Col>
              <Col md={8}>
                <div className={classes.scrollTop}>
                  <img
                    src={"/images/paper-top.png"}
                    className={classes.taskScroll}
                  />
                </div>
                <div className={[classes.scrollMiddle, classes.taskScroll].join(" ")}>
                  <div className={classes.taskBox}>
                    <Row>
                      <Col
                        className={classes.taskContainer}
                        dangerouslySetInnerHTML={{__html: this.state.tasks.length ? this.state.tasks[this.state.currentTask].text : ""}}>
                      </Col>
                    </Row>
                  </div>
                </div>
                <div className={classes.scrollBottom}>
                  <img
                    src={"/images/paper-bottom.png"}
                    className={classes.taskScroll}
                  />
                </div>
                <Row className={"mt-3"}>
                  <Col>
                    <FormControl
                      placeholder="Ответ"
                      onChange={(event) => this.setState({currentAnswer: event.target.value})}
                    />
                  </Col>
                </Row>
                <Row className={"mt-3"}>
                  <Col className={classes.buttonContainer}>
                    <Button onClick={this.processSubmitClick}>Дальше!</Button>
                  </Col>
                </Row>
              </Col>
              <Col md={2}>
                <div className={classes.battleProgress}>
                  {this.state.opponentTasksStatuses.map((status, ind) => {
                    switch (status) {
                      case "inactive":
                        return (
                          <Fragment>
                            <img
                              src={"/images/battleProgress/" + (ind === 0 ? "chain-start.png" : "chain-middle.png")}
                              className={classes.chain}
                            />
                            <div className={classes.ballInactive}>{ind + 1}</div>
                          </Fragment>
                        );
                      case "success":
                        return (
                          <Fragment>
                            <img
                              src={"/images/battleProgress/" + (ind === 0 ? "chain-start.png" : "chain-middle.png")}
                              className={classes.chain}
                            />
                            <div className={classes.ballSuccess}>{ind + 1}</div>
                          </Fragment>
                        );
                      case "fail":
                        return (
                          <Fragment>
                            <img
                              src={"/images/battleProgress/" + (ind === 0 ? "chain-start.png" : "chain-middle.png")}
                              className={classes.chain}
                            />
                            <div className={classes.ballFail}>{ind + 1}</div>
                          </Fragment>
                        );
                    }
                  })}
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        {this.state.modalIsOpen ? (
          <div className={classes.modalContainer}>

            <div className={classes.modalGroup}>
              <div className={classes.hat}>
                <img
                  src={process.env.PUBLIC_URL + "/images/hat.png"}
                  alt={"hat"}
                  className={classes.modal}
                />
              </div>
              <div className={classes.scrollTop}>
                <img src={"/images/scrollTop.png"} className={classes.modal}/>
              </div>
              <div className={[classes.scrollMiddleModal, classes.modal].join(" ")}>
                <div className={classes.modalBox}>
                  {this.state.battleType === "training" ? (
                    "Хорошо потренировались! Приходи ещё!"
                  ) : (this.state.winner ? (
                    "Ты победил! Так держать!"
                  ) : (
                    "Какая досада, ты проиграл... Тренируйся усерднее!"
                  ))}
                  <LinkContainer to={"/character"}>
                    <Button className="mt-3">До следующей встречи!</Button>
                  </LinkContainer>
                </div>
              </div>
              <div className={classes.scrollBottom}>
                <img src={"/images/scrollBottom.png"} className={classes.modal}/>
              </div>
            </div>
          </div>
        ) : ""}
      </>
    );
  }
}

export const Battle = withRouter(BattlePage);
