import React, {useState, useEffect} from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import cookies from 'js-cookie';

import {InputGroup, FormControl, Button} from "react-bootstrap";

import classes from './Login.module.scss';

const LoginPage = props => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const processLogin = async () => {
    try {
      const response = await axios.post('https://digital-vanguard.herokuapp.com/login', {
        username, password
      });
      cookies.set('token', response.data.access_token);
      props.history.push('/character');
    } catch (e) {
      console.log('Auth failed', e);
    }

  };

  return (
    <div className={classes.loginContainer}>
      <div className={classes.loginGroup}>
        <div className={classes.hat}>
          <img src={process.env.PUBLIC_URL + "/images/hat.png"} alt={"hat"}/>
        </div>
        <div className={classes.scrollTop}>
          <img src={"/images/scrollTop.png"} />
        </div>
        <div className={classes.scrollMiddle}>
          <div className={classes.loginBox}>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="login">Логин</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="Логин"
                onChange={event => setUsername(event.target.value)}
              />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="password">Пароль</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="Пароль"
                onChange={event => setPassword(event.target.value)}
              />
            </InputGroup>
            <Button onClick={processLogin}>Войти</Button>
          </div>
        </div>
        <div className={classes.scrollBottom}>
          <img src={"/images/scrollBottom.png"} />
        </div>
      </div>
    </div>
  );
};

export const Login = withRouter(LoginPage);
